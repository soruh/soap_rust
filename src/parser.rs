use std::{
    convert::Infallible,
    fmt::{Debug, Display},
    str::FromStr,
};

use roxmltree::{Document, Node, NodeType};

pub struct NodeDebugger<'a, 'b> {
    node: Node<'a, 'b>,
}

impl<'a, 'b> std::fmt::Debug for NodeDebugger<'a, 'b> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        struct Equality<A: std::fmt::Display, B: std::fmt::Debug>(A, B);

        impl<A: std::fmt::Display, B: std::fmt::Debug> std::fmt::Debug for Equality<A, B> {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                write!(f, "{}={:?}", self.0, self.1)
            }
        }

        match self.node.node_type() {
            NodeType::Element => {
                let attributes = self
                    .node
                    .attributes()
                    .iter()
                    .map(|attribute| Equality(attribute.name(), attribute.value()))
                    .collect::<Vec<_>>();

                let children = self
                    .node
                    .children()
                    .filter(|node| {
                        node.node_type() != NodeType::Text
                            || !node.text().unwrap().trim_end().is_empty()
                    })
                    .map(|node| NodeDebugger { node })
                    .collect::<Vec<_>>();

                let namespace = self.node.tag_name().namespace().and_then(|uri| {
                    (self.node.lookup_namespace_uri(None) != Some(uri))
                        .then(|| self.node.lookup_prefix(uri).unwrap())
                });

                let name = if let Some(namespace) = namespace {
                    format!("{}:{}", namespace, self.node.tag_name().name())
                } else {
                    self.node.tag_name().name().to_owned()
                };

                if attributes.is_empty() {
                    write!(f, "{}", name)?;
                } else {
                    write!(f, "{}{:?}", name, attributes)?;
                }

                if !children.is_empty() {
                    let body = format!("{:#?}", children);
                    write!(f, " {{{}}}", &body[1..body.len() - 1])?;
                }

                Ok(())
            }
            NodeType::Comment => {
                let body = self.node.text().unwrap();
                write!(f, "// {}", format!("{:?}", &body[1..body.len() - 1]))
            }
            NodeType::Text => {
                write!(f, "{:?}", self.node.text().unwrap())
            }
            _ => self.node.fmt(f),
        }
    }
}

#[allow(dead_code)]
fn print_node(node: Node) {
    eprintln!("{:#?}", NodeDebugger { node });
}

fn is_element(kind: &'static str) -> impl Fn(&Node) -> bool {
    _is_element(kind, None)
}

fn is_element_with_prefix(prefix: &'static str, kind: &'static str) -> impl Fn(&Node) -> bool {
    _is_element(kind, Some(prefix))
}

fn _is_element(kind: &'static str, prefix: Option<&'static str>) -> impl Fn(&Node) -> bool {
    move |node: &Node| {
        node.is_element()
            && node.tag_name().name() == kind
            && node.tag_name().namespace() == node.lookup_namespace_uri(prefix)
    }
}

macro_rules! missing_attr {
    ($name: literal $(,$tail: literal)*) => {{
        let message = concat!(
            $($tail, "<-"),*
        );
        panic!("missing attribute {:?} at {}", $name, &message[..message.len()-2])
    }};
}

#[derive(Debug)]
pub struct Import {
    namespace: String,
}

impl Import {
    fn parse(node: Node) -> Self {
        let namespace = node
            .attribute("namespace")
            .unwrap_or_else(|| missing_attr!("namespace", "xsd:import", "type"))
            .to_owned();

        Self { namespace }
    }
}

#[derive(Debug)]
pub struct Restriction {
    pub base: QName,
    pub body: Vec<(QName, Vec<(String, String)>)>,
}

impl Restriction {
    fn parse(node: Node) -> Self {
        let base = node
            .attribute("base")
            .unwrap_or_else(|| missing_attr!("base", "restriction", "complexType", "type"))
            .parse()
            .unwrap();

        let body = node
            .children()
            .filter(|node| node.is_element())
            .map(|node| {
                (
                    node.tag_name().name().parse().unwrap(),
                    node.attributes()
                        .iter()
                        .map(|attribute| {
                            (attribute.name().to_owned(), attribute.value().to_owned())
                        })
                        .collect(),
                )
            })
            .collect();

        Restriction { base, body }
    }
}

#[derive(Debug)]
pub struct ComplexContent {
    pub restrictions: Vec<Restriction>,
}

#[derive(Debug)]
pub struct Element {
    pub name: String,
    pub r#type: QName,
}

impl Element {
    fn parse(node: Node) -> Self {
        let name = node
            .attribute("name")
            .unwrap_or_else(|| missing_attr!("name", "complexType", "type"))
            .to_owned();

        let r#type = node
            .attribute("type")
            .unwrap_or_else(|| missing_attr!("type", "all", "complexType", "type"))
            .parse()
            .unwrap();

        Element { name, r#type }
    }
}

#[derive(Debug)]
pub enum ComplexBody {
    ComplexContent(ComplexContent),
    All(Vec<Element>),
}

impl ComplexBody {
    fn parse(node: Node) -> Self {
        if is_element_with_prefix("xsd", "complexContent")(&node) {
            let restrictions = node
                .children()
                .filter(is_element_with_prefix("xsd", "restriction"))
                .map(Restriction::parse)
                .collect();

            // TODO: parse other content

            ComplexBody::ComplexContent(ComplexContent { restrictions })
        } else if is_element_with_prefix("xsd", "all")(&node) {
            let elements = node
                .children()
                .filter(is_element_with_prefix("xsd", "element"))
                .map(Element::parse)
                .collect();

            ComplexBody::All(elements)
        } else {
            todo!("other complex content")
        }
    }
}

#[derive(Debug)]
pub struct ComplexType {
    pub name: String,
    pub body: ComplexBody,
}

impl ComplexType {
    fn parse(node: Node) -> Self {
        let name = node
            .attribute("name")
            .unwrap_or_else(|| missing_attr!("name", "complexType", "type"))
            .to_owned();

        let body = ComplexBody::parse(node.children().find(|node| node.is_element()).unwrap());

        ComplexType { name, body }
    }
}

#[derive(Debug)]
pub struct Types {
    pub target_namespace: String,
    pub imports: Vec<Import>,
    pub complex_types: Vec<ComplexType>,
}
impl Types {
    fn parse(node: Node) -> Self {
        let target_namespace = node
            .attribute("targetNamespace")
            .unwrap_or_else(|| missing_attr!("targetNamespace", "type"))
            .to_owned();

        let imports = node
            .children()
            .filter(is_element_with_prefix("xsd", "import"))
            .map(Import::parse)
            .collect();

        let complex_types = node
            .children()
            .filter(is_element_with_prefix("xsd", "complexType"))
            .map(ComplexType::parse)
            .collect();

        Self {
            target_namespace,
            imports,
            complex_types,
        }
    }
}

#[derive(Debug)]
pub struct Part {
    pub name: String,
    pub r#type: QName,
}

impl Part {
    fn parse(node: Node) -> Self {
        let name = node
            .attribute("name")
            .unwrap_or_else(|| missing_attr!("name", "part", "message"))
            .to_owned();

        let r#type = node
            .attribute("type")
            .unwrap_or_else(|| missing_attr!("type", "part", "message"))
            .parse()
            .unwrap();

        Self { name, r#type }
    }
}

#[derive(Debug)]
pub struct Message {
    pub name: String,
    pub parts: Vec<Part>,
}
impl Message {
    fn parse(node: Node) -> Self {
        let name = node
            .attribute("name")
            .unwrap_or_else(|| missing_attr!("name", "message"))
            .to_owned();

        let parts = node
            .children()
            .filter(is_element("part"))
            .map(Part::parse)
            .collect();

        Self { name, parts }
    }
}

#[derive(Debug)]
pub struct SoapAction {
    scope: String,
    operation: String,
}

impl FromStr for SoapAction {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut iter = s.split('#');

        let scope = iter.next().unwrap().to_owned();
        let operation = iter.next().unwrap().to_owned();
        assert!(iter.next().is_none());

        Ok(SoapAction { scope, operation })
    }
}

#[derive(Debug)]
pub struct BindingOperation {
    pub name: String,
    pub style: Style,
    pub soap_action: SoapAction,
    pub input: Option<SoapBody>,
    pub output: Option<SoapBody>,
}

#[derive(Debug)]
pub struct SoapBody {
    pub parts: Option<String>,
    pub r#use: Option<String>,
    pub namespace: Option<String>,
    pub encoding_style: Option<String>,
}

#[derive(Debug)]
pub enum Style {
    Rpc,
}

impl FromStr for Style {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s == "rpc" {
            Ok(Self::Rpc)
        } else {
            Err(format!("unknown style: {:?}", s))
        }
    }
}

impl BindingOperation {
    fn parse(node: Node) -> Self {
        // print_node(node);

        let name = node
            .attribute("name")
            .unwrap_or_else(|| missing_attr!("name", "operation", "binding"))
            .to_owned();

        let soap_operation = node
            .children()
            .find(is_element_with_prefix("soap", "operation"))
            .unwrap();

        let soap_action = soap_operation
            .attribute("soapAction")
            .unwrap_or_else(|| {
                missing_attr!("soapAction", "soap:operation", "operation", "binding")
            })
            .to_owned()
            .parse()
            .unwrap();

        let style = soap_operation
            .attribute("style")
            .unwrap_or_else(|| missing_attr!("style", "soap:operation", "operation", "binding"))
            .to_owned()
            .parse()
            .unwrap();

        let get_soap_body = |node: Node| {
            let body = node
                .children()
                .find(is_element_with_prefix("soap", "body"))
                .unwrap_or_else(|| {
                    missing_attr!(
                        "soap:body",
                        "input/output",
                        "soap:operation",
                        "operation",
                        "binding"
                    )
                });

            let parts = body.attribute("parts").map(|x| x.to_owned());
            let r#use = body.attribute("use").map(|x| x.to_owned());
            let namespace = body.attribute("namespace").map(|x| x.to_owned());
            let encoding_style = body.attribute("encoding_tyle").map(|x| x.to_owned());

            SoapBody {
                parts,
                r#use,
                namespace,
                encoding_style,
            }
        };

        let input = node.children().find(is_element("input")).map(get_soap_body);

        let output = node
            .children()
            .find(is_element("output"))
            .map(get_soap_body);

        BindingOperation {
            name,
            soap_action,
            style,
            input,
            output,
        }
    }
}

#[derive(Clone)]
pub struct QName {
    pub prefix: Option<String>,
    pub name: String,
}

impl std::fmt::Debug for QName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if let Some(prefix) = &self.prefix {
            write!(f, "{:?}:{:?}", prefix, self.name)
        } else {
            Debug::fmt(&self.name, f)
        }
    }
}

impl std::fmt::Display for QName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if let Some(prefix) = &self.prefix {
            write!(f, "{}:{}", prefix, self.name)
        } else {
            Display::fmt(&self.name, f)
        }
    }
}

impl FromStr for QName {
    type Err = Infallible;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(QName::new(s))
    }
}

impl QName {
    fn new(body: &str) -> Self {
        if let Some(index) = body.find(':') {
            Self {
                prefix: Some(body[..index].to_owned()),
                name: body[index + 1..].to_owned(),
            }
        } else {
            Self {
                prefix: None,
                name: body.to_owned(),
            }
        }
    }
}

#[derive(Debug)]
pub struct Binding {
    pub name: String,
    pub r#type: QName,
    pub style: Style,
    pub transport: String,
    pub operations: Vec<BindingOperation>,
}
impl Binding {
    fn parse(node: Node) -> Self {
        let name = node
            .attribute("name")
            .unwrap_or_else(|| missing_attr!("name", "binding"))
            .to_owned();

        let r#type = node
            .attribute("type")
            .unwrap_or_else(|| missing_attr!("type", "binding"))
            .parse()
            .unwrap();

        let soap_binding = node
            .children()
            .find(is_element_with_prefix("soap", "binding"))
            .unwrap();

        let style = soap_binding
            .attribute("style")
            .unwrap_or_else(|| missing_attr!("style", "soap:binding", "binding"))
            .to_owned()
            .parse()
            .unwrap();

        let transport = soap_binding
            .attribute("transport")
            .unwrap_or_else(|| missing_attr!("transport", "soap:binding", "binding"))
            .to_owned();

        let operations = node
            .children()
            .filter(is_element("operation"))
            .map(BindingOperation::parse)
            .collect();

        Self {
            name,
            r#type,
            style,
            transport,
            operations,
        }
    }
}

#[derive(Debug)]
pub struct Port {
    pub name: String,
    pub binding: QName,
    pub addresses: Vec<String>,
}

impl Port {
    fn parse(node: Node) -> Self {
        let name = node
            .attribute("name")
            .unwrap_or_else(|| missing_attr!("name", "port", "service"))
            .to_owned();

        let binding = node
            .attribute("binding")
            .unwrap_or_else(|| missing_attr!("binding", "port", "service"))
            .parse()
            .unwrap();

        let addresses = node
            .children()
            .filter(is_element_with_prefix("soap", "address"))
            .map(|node| {
                node.attribute("location")
                    .unwrap_or_else(|| missing_attr!("location", "address", "port", "service"))
                    .to_owned()
            })
            .collect();

        Self {
            name,
            binding,
            addresses,
        }
    }
}

#[derive(Debug)]
pub struct Service {
    pub name: String,
    pub ports: Vec<Port>,
}
impl Service {
    fn parse(node: Node) -> Self {
        let name = node
            .attribute("name")
            .unwrap_or_else(|| missing_attr!("name", "service"))
            .to_owned();

        let ports = node
            .children()
            .filter(is_element("port"))
            .map(Port::parse)
            .collect();

        Self { name, ports }
    }
}

#[derive(Debug)]
pub struct Operation {
    pub name: String,
    pub documentation: Option<String>,
    pub input: Option<QName>,
    pub output: Option<QName>,
    pub faults: Vec<QName>,
}

impl Operation {
    pub fn parse(node: Node) -> Self {
        let name = node
            .attribute("name")
            .unwrap_or_else(|| missing_attr!("name", "operation", "portType"))
            .to_owned();

        let documentation = node
            .children()
            .find(is_element("documentation"))
            .map(|node| node.text().unwrap().to_owned());

        let input = node.children().find(is_element("input")).map(|node| {
            node.attribute("message")
                .unwrap_or_else(|| missing_attr!("message", "input", "operation", "portType"))
                .parse()
                .unwrap()
        });

        let output = node.children().find(is_element("output")).map(|node| {
            node.attribute("message")
                .unwrap_or_else(|| missing_attr!("message", "output", "operation", "portType"))
                .parse()
                .unwrap()
        });

        let faults = node
            .children()
            .filter(is_element("fault"))
            .map(|_| todo!("parse faults"))
            .collect();

        Self {
            name,
            documentation,
            input,
            output,
            faults,
        }
    }
}

#[derive(Debug)]
pub struct PortType {
    pub name: String,
    pub operations: Vec<Operation>,
}

impl PortType {
    pub fn parse(node: Node) -> Self {
        let name = node
            .attribute("name")
            .unwrap_or_else(|| missing_attr!("name", "portType"))
            .to_owned();

        let operations = node
            .children()
            .filter(is_element("operation"))
            .map(Operation::parse)
            .collect();

        Self { name, operations }
    }
}

#[derive(Debug)]
pub struct WsdlFile {
    pub target_namespace: String,
    pub port_types: Vec<PortType>,
    pub services: Vec<Service>,
    pub bindings: Vec<Binding>,
    pub types: Types,
    pub messages: Vec<Message>,
    pub soap_env: String,
    pub xsi: String,
    pub xsd: String,
}

impl WsdlFile {
    pub fn parse(string: &str) -> Self {
        let document = Document::parse(string).unwrap();

        let namespaces = document.root_element().namespaces();
        let namespace = |name: &'static str| {
            namespaces
                .iter()
                .find(|namespace| namespace.name() == Some(name))
                .unwrap()
                .uri()
                .to_owned()
        };

        let soap_env = namespace("SOAP-ENV");
        let xsi = namespace("xsi");
        let xsd = namespace("xsd");

        let definitions = document
            .descendants()
            .find(is_element("definitions"))
            .unwrap();

        let target_namespace = definitions
            .attribute("targetNamespace")
            .unwrap_or_else(|| missing_attr!("targetNamespace"))
            .to_owned();

        let types = definitions
            .children()
            .find(is_element("types"))
            .unwrap()
            .children()
            .find(is_element_with_prefix("xsd", "schema"))
            .map(Types::parse)
            .unwrap();

        let messages = definitions
            .children()
            .filter(is_element("message"))
            .map(Message::parse)
            .collect();

        let bindings = definitions
            .children()
            .filter(is_element("binding"))
            .map(Binding::parse)
            .collect();

        let services = definitions
            .children()
            .filter(is_element("service"))
            .map(Service::parse)
            .collect();

        let port_types = definitions
            .children()
            .filter(is_element("portType"))
            .map(PortType::parse)
            .collect();

        Self {
            target_namespace,
            port_types,
            services,
            bindings,
            types,
            messages,
            soap_env,
            xsi,
            xsd,
        }
    }
}

#[test]
fn test_parser() {
    use std::io::Read;

    let mut input = String::new();
    std::fs::File::open("ilias.wsdl")
        .unwrap()
        .read_to_string(&mut input)
        .unwrap();

    WsdlFile::parse(&input);
}
