use case::CaseExt;
use codegen::{Module, Type};
use std::{borrow::Cow, collections::HashMap};

use crate::parser::{ComplexBody, QName, WsdlFile};

#[derive(Debug)]
pub struct Generator {
    wsdl: WsdlFile,
}

#[derive(Debug, Default)]
pub struct State {
    pub default_scope: String,
    scopes: HashMap<String, HashMap<String, String>>,
}

impl State {
    pub fn new() -> Self {
        let mut state = Self::default();

        let xsd: HashMap<String, String> = [
            // A signed 8 bit integer
            ("byte", "i8"),
            // A signed 32 bit integer
            ("int", "i32"),
            // A signed 64 bit integer
            ("long", "i64"),
            // A signed 16 bit integer
            ("short", "i16"),
            // An unsigned 8 bit integer
            ("unsignedByte", "u8"),
            // An unsigned 16 bit integer
            ("unsignedShort", "u16"),
            // An unsigned 32 bit integer
            ("unsignedInt", "u32"),
            // An unsigned 64 bit integer
            ("unsignedLong", "u64"),
            ("float", "f32"),
            ("double", "f64"),
            ("boolean", "bool"),
            ("string", "String"),
        ]
        .iter()
        .copied()
        .map(|(k, v)| (k.to_owned(), v.to_owned()))
        .collect();

        state.scopes.insert("xsd".to_owned(), xsd);

        state
    }

    fn scope_name(&self, name: Option<String>) -> String {
        match name.as_deref() {
            Some(inner) if inner != "tns" => name.unwrap(),
            _ => self.default_scope.clone(),
        }
    }

    fn scope(&mut self, name: Option<String>) -> &mut HashMap<String, String> {
        self.scopes
            .entry(self.scope_name(name))
            .or_insert_with(HashMap::new)
    }

    fn insert(&mut self, namespace: Option<String>, key: String, value: String) -> Option<String> {
        self.scope(namespace).insert(key, value)
    }

    fn lookup_ident(&self, name: &QName) -> Result<&String, String> {
        self.scopes
            .get(&self.scope_name(name.prefix.clone()))
            .ok_or_else(|| format!("could not find scope {:?}", name.prefix))?
            .get(&name.name)
            .ok_or_else(|| {
                format!(
                    "could not find type with identifier {} in scope ({})",
                    name.name,
                    name.prefix.as_deref().unwrap_or(" "),
                )
            })
    }
}

impl Generator {
    pub fn new(wsdl: WsdlFile) -> Self {
        Self { wsdl }
    }

    pub fn generate(&self, state: &mut State) -> codegen::Scope {
        state.default_scope = self.wsdl.target_namespace.clone();

        let mut scope = codegen::Scope::new();

        scope
            .new_fn("field")
            .generic("'a, 'b")
            .arg("node", "::roxmltree::Node<'a, 'b>")
            .arg("tag", "&'static str")
            .ret("::roxmltree::Node<'a, 'b>")
            .line(r#"node.children().find(|node| node.tag_name().name() == tag).unwrap_or_else(|| panic!("expected a {:?} child", tag))"#);

        scope
            .new_fn("is_element")
            .vis("pub")
            .arg("kind", "&'static str")
            .ret("impl Fn(&::roxmltree::Node) -> bool")
            .line("_is_element(kind, None)");

        scope
            .new_fn("is_element_with_prefix")
            .vis("pub")
            .arg("prefix", "&'static str")
            .arg("kind", "&'static str")
            .ret("impl Fn(&::roxmltree::Node) -> bool")
            .line("_is_element(kind, Some(prefix))");

        scope
            .new_fn("_is_element")
            .arg("kind", "&'static str")
            .arg("prefix", "Option<&'static str>")
            .ret("impl Fn(&::roxmltree::Node) -> bool")
            .line(
                r#"move |node: &::roxmltree::Node| {
                    node.is_element()
                        && node.tag_name().name() == kind
                        && node.tag_name().namespace() == node.lookup_namespace_uri(prefix)
                }"#,
            );

        scope
            .new_struct("SoapFault")
            .vis("pub")
            .derive("Debug")
            .field("code", "String")
            .field("message", "String");

        let parse_soap_fault = r#"
            let document = ::roxmltree::Document::parse(&fault).unwrap();
            let envelope = document.root().children().find(is_element_with_prefix("SOAP-ENV", "Envelope")).unwrap();
            let body = envelope.children().find(is_element_with_prefix("SOAP-ENV", "Body")).unwrap();
            let fault = body.children().find(is_element_with_prefix("SOAP-ENV", "Fault")).unwrap();
            let message = fault.children().find(is_element("faultstring")).unwrap().text().unwrap().to_owned();
            let code = fault.children().find(is_element("faultcode")).unwrap().text().unwrap().to_owned();
            let code = code.find(':').map(|i| code[i+1..].to_owned()).unwrap_or(code);

            Self {
                code,
                message,
            }
        "#;

        scope
            .new_impl("SoapFault")
            .new_fn("parse")
            .arg("fault", "String")
            .ret("Self")
            .line(parse_soap_fault);

        let soap_error = scope.new_enum("SoapError").vis("pub"); // .derive("Debug");

        soap_error.new_variant("Io(::reqwest::Error)");
        soap_error.new_variant("Fault(SoapFault)");

        scope
            .new_impl("SoapError")
            .impl_trait("From<::reqwest::Error>")
            .new_fn("from")
            .arg("err", "::reqwest::Error")
            .ret("Self")
            .line("Self::Io(err)");

        scope
            .new_impl("SoapError")
            .impl_trait("std::fmt::Display")
            .new_fn("fmt")
            .arg_ref_self()
            .arg("f", "&mut ::std::fmt::Formatter")
            .ret("::std::fmt::Result")
            .line(r#"match self { SoapError::Fault(fault) => write!(f, "Fault({}): {:?}", fault.code, fault.message), SoapError::Io(err) => err.fmt(f) }"#);

        scope
            .new_impl("SoapError")
            .impl_trait("std::fmt::Debug")
            .new_fn("fmt")
            .arg_ref_self()
            .arg("f", "&mut ::std::fmt::Formatter")
            .ret("::std::fmt::Result")
            .line(r#"write!(f, "{}", self)"#);

        scope.new_impl("SoapError").impl_trait("std::error::Error");

        scope.push_module(self.generate_types(state));
        scope.push_module(self.generate_messages(state));
        scope.push_module(self.generate_services(state));

        scope
    }

    fn generate_types(&self, state: &mut State) -> Module {
        let mut types = Module::new("types");
        types.vis("pub");

        let namespace = &self.wsdl.types.target_namespace;

        let old_default_scope = std::mem::replace(&mut state.default_scope, namespace.clone());

        for complex_type in &self.wsdl.types.complex_types {
            let ident = quote_literals(&complex_type.name).to_camel();

            let r#struct = types
                .new_struct(&ident)
                .vis("pub")
                .derive("Clone")
                .derive("Debug");

            state.insert(
                Some(namespace.clone()),
                complex_type.name.clone(),
                ident.clone(),
            );

            match &complex_type.body {
                ComplexBody::ComplexContent(body) => {
                    assert_eq!(
                        body.restrictions.len(),
                        1,
                        "more restrictions than expected"
                    );

                    let restriction = &body.restrictions[0];

                    assert_eq!(
                        restriction.base.prefix.as_deref(),
                        Some("SOAP-ENC"),
                        "unknown restriction"
                    );
                    assert_eq!(restriction.base.name, "Array", "unknown restriction");

                    assert_eq!(
                        restriction.body.len(),
                        1,
                        "more restriction items than expected"
                    );

                    let (item_kind, attributes) = &restriction.body[0];

                    assert_eq!(item_kind.prefix, None);
                    assert_eq!(item_kind.name, "attribute");

                    assert_eq!(
                        attributes.len(),
                        2,
                        "expected exactly a 'ref' and an 'arrayType' attribute"
                    );

                    let r#ref = attributes
                        .iter()
                        .find_map(|(key, value)| (key == "ref").then(|| value))
                        .unwrap();

                    assert_eq!(r#ref, "SOAP-ENC:arrayType");

                    let mut array_type: QName = attributes
                        .iter()
                        .find_map(|(key, value)| (key == "arrayType").then(|| value))
                        .unwrap()
                        .parse()
                        .unwrap();

                    let name = array_type.name.strip_suffix("[]").unwrap().to_owned();
                    array_type.name = name;

                    let r#type = state.lookup_ident(&array_type).unwrap();

                    r#struct.tuple_field(&format!("pub Vec<{}>", r#type));
                }
                ComplexBody::All(body) => {
                    for field in body {
                        let r#type = Type::new(state.lookup_ident(&field.r#type).unwrap());

                        r#struct.field(&format!("pub {}", field.name), r#type);
                    }
                }
            }
        }

        state.default_scope = old_default_scope;

        types
    }

    fn generate_messages(&self, state: &mut State) -> Module {
        let mut messages = Module::new("messages");
        messages.vis("pub");
        messages.import("super::types", "*");

        for message in &self.wsdl.messages {
            let ident = quote_literals(&message.name).into_owned().to_camel();

            let r#struct = messages
                .new_struct(&ident)
                .vis("pub")
                .derive("Clone")
                .derive("Debug");

            state.insert(None, message.name.clone(), ident);

            for part in &message.parts {
                let field_name = quote_literals(&part.name);

                let r#type = state.lookup_ident(&part.r#type).unwrap();

                r#struct.field(&format!("pub {}", field_name), r#type);
            }
        }

        messages
    }

    /*
    fn generate_port_types(&self, state: &mut State) -> Module {
        // TODO: we probably don't want to generate these but rather have them be
        // methods on the binding
        let mut port_types = Module::new("port_types");
        port_types.vis("pub");
        port_types.import("super::messages", "*");

        for port_type in &self.wsdl.port_types {
            let module = port_types.new_module(&port_type.name);

            for operation in &port_type.operations {
                let ident = quote_literals(&operation.name);
                let function = module.new_fn(&ident).vis("pub");
                if let Some(input) = &operation.input {
                    function.arg("input", state.lookup_ident(&input).unwrap());
                }
                if let Some(output) = &operation.output {
                    function.ret(state.lookup_ident(&output).unwrap());
                }
                if let Some(documentation) = &operation.documentation {
                    function.doc(&documentation);
                }

                function.line("foo\nbar");
            }
        }

        port_types
    }
    */

    fn build_parser(&self, state: &mut State, r#type: &QName) -> String {
        match (r#type.prefix.as_deref(), r#type.name.as_str()) {
            (Some("xsd"), "string") => r#"node.text().unwrap().to_owned()"#.to_owned(),
            (Some("xsd"), "boolean") => r#"node
                    .text()
                    .unwrap()
                    .parse::<bool>()
                    .ok()
                    .or_else(||
                        res.parse::<u8>()
                        .ok()
                        .and_then(|x| match x {
                            0 => Some(false),
                            1 => Some(true),
                            _ => None,
                        })
                    )
                    .expect("failed to parse boolean")"#
                .to_owned(),
            (Some("xsd"), "int") => {
                r#"node.text().unwrap().parse().expect("failed to parse integer")"#.to_owned()
            }
            (Some("xsd"), "double") => {
                r#"node.text().unwrap().parse().expect("failed to parse integer")"#.to_owned()
            }
            (Some(ns), type_name) if ns == "tns" || ns == self.wsdl.target_namespace => {
                let complex_type = self
                    .wsdl
                    .types
                    .complex_types
                    .iter()
                    .find(|complex_type| complex_type.name == type_name)
                    .unwrap();

                let ident = state.lookup_ident(r#type).unwrap().clone();

                let body: String = match &complex_type.body {
                    ComplexBody::All(fields) => format!(
                        "{{{}}}",
                        fields
                            .iter()
                            .map(|field| {
                                format!(
                                    "{field_name}: {{
                                        let node = super::field(node, {field_name:?});
                                        {value}
                                    }},",
                                    field_name = field.name,
                                    value = self.build_parser(state, &field.r#type)
                                )
                            })
                            .collect::<String>()
                    ),
                    ComplexBody::ComplexContent(content) => {
                        assert_eq!(
                            content.restrictions.len(),
                            1,
                            "expected exactly one restriction"
                        );
                        let restriction = content.restrictions.first().unwrap();

                        assert_eq!(
                            (
                                restriction.base.prefix.as_deref(),
                                restriction.base.name.as_str()
                            ),
                            (Some("SOAP-ENC"), "Array"),
                            "can only work with array restrictions for now"
                        );
                        assert_eq!(restriction.body.len(), 1, "expected exactly one attribute");

                        let attributes = restriction
                            .body
                            .iter()
                            .find_map(|(k, v)| (k.name == "attribute").then(|| v))
                            .unwrap();

                        let r#ref = attributes
                            .iter()
                            .find_map(|(k, v)| (k == "ref").then(|| v))
                            .unwrap();

                        assert_eq!(
                            r#ref, "SOAP-ENC:arrayType",
                            "can only work with array restrictions for now"
                        );

                        let array_type = attributes
                            .iter()
                            .find_map(|(k, v)| (k == "arrayType").then(|| v))
                            .unwrap();

                        let array_type: QName =
                            array_type.strip_suffix("[]").unwrap().parse().unwrap();

                        format!(
                            r#"(node.children().filter(is_element("item")).map(|node| {}).collect())"#,
                            self.build_parser(state, &array_type)
                        )
                    }
                };

                format!(
                    r#"super::types::{ident} {body}"#,
                    ident = ident,
                    body = body,
                )
            }

            _ => {
                todo!("build parser for {:?}", r#type)
            }
        }
    }

    fn generate_services(&self, state: &mut State) -> Module {
        let mut services = Module::new("services");
        services.vis("pub");
        services.import("super::messages", "*");
        services.import("super", "{ is_element, is_element_with_prefix }");

        for service in &self.wsdl.services {
            // TODO: allow for more that one port
            let port = service.ports.first().unwrap();

            let address = port.addresses.first().unwrap();

            let binding = self
                .wsdl
                .bindings
                .iter()
                .find(|bindings| bindings.name == port.binding.name)
                .unwrap();

            let port_type = self
                .wsdl
                .port_types
                .iter()
                .find(|port_type| port_type.name == binding.r#type.name)
                .unwrap();

            let r#struct = services.new_struct(&service.name).vis("pub");

            r#struct.field("client", "::reqwest::Client");

            let r#impl = services.new_impl(&service.name);

            r#impl
                .new_fn("url")
                .vis("pub")
                .ret("&'static str")
                .line(format!("{:?}", address));

            let send_soap_message = format!(
                r##"
let message = format!(r#"<?xml version='1.0' encoding='UTF-8'?>
    <SOAP-ENV:Envelope xmlns:SOAP-ENV={soap_env:?} xmlns:xsi={xsi:?} xmlns:xsd={xsd:?} xmlns:ns1={ns1:?}>
    <SOAP-ENV:Body>
        {{}}
    </SOAP-ENV:Body>
    </SOAP-ENV:Envelope>
"#, body);

let response = self.client
    .post({url:?})
    .header("Content-Type", "text/xml; charset=utf-8")
    .body(message)
    .send()
    .await?;

let success = response.error_for_status_ref().is_ok();

let text = response.text().await?;

if success {{
    Ok(text)
}} else {{
    Err(super::SoapError::Fault(super::SoapFault::parse(text)))
}}
    
"##,
                soap_env = self.wsdl.soap_env,
                xsi = self.wsdl.xsi,
                xsd = self.wsdl.xsd,
                ns1 = self.wsdl.target_namespace,
                url = address
            );

            /*
            <ns1:getPriceResponse
                xmlns:ns1 = "urn:examples:priceservice"
                SOAP-ENV:encodingStyle = "http://www.w3.org/2001/12/soap-encoding">
                <return xsi:type = "xsd:double">54.99</return>
            </ns1:getPriceResponse>
            */

            r#impl
                .new_fn("send_soap_message")
                .set_async(true)
                .arg_ref_self()
                .vis("pub")
                .arg("body", "&str")
                .ret("Result<String, super::SoapError>")
                .line(send_soap_message);

            r#impl
                .new_fn("new")
                .vis("pub")
                .ret("Self")
                .line("Self::with_client(::reqwest::Client::new())");

            r#impl
                .new_fn("with_client")
                .vis("pub")
                .arg("client", "::reqwest::Client")
                .ret("Self")
                .line("Self { client }");

            for binding_operation in &binding.operations {
                let operation = port_type
                    .operations
                    .iter()
                    .find(|operation| operation.name == binding_operation.name)
                    .unwrap();

                let method = r#impl
                    .new_fn(&operation.name.to_snake())
                    .vis("pub")
                    .set_async(true)
                    .arg_ref_self();

                if let Some(documentation) = &operation.documentation {
                    method.doc(documentation);
                }

                if let Some(input) = &operation.input {
                    method.arg("input", state.lookup_ident(input).unwrap());
                }

                let output_type = operation.output.clone().unwrap();

                let return_type = state.lookup_ident(&output_type).unwrap().clone();

                method.ret(format!(
                    "Result<{}, super::SoapError>",
                    return_type.to_camel()
                ));

                let input = binding_operation.input.as_ref().unwrap();

                assert_eq!(input.r#use.as_deref(), Some("encoded"));

                let encoding_style = binding_operation
                    .input
                    .as_ref()
                    .map(|input| input.encoding_style.as_deref())
                    .flatten();

                let message_name = operation.input.as_ref().unwrap();
                let input_message = self
                    .wsdl
                    .messages
                    .iter()
                    .find(|message| message.name == message_name.name)
                    .unwrap();

                let args: String = input_message
                    .parts
                    .iter()
                    .map(|part| {
                        let complex_type = self
                            .wsdl
                            .types
                            .complex_types
                            .iter()
                            .find(|t| t.name == part.r#type.name);

                        let value = if let Some(complex_type) = complex_type {
                            match &complex_type.body {
                                ComplexBody::ComplexContent(content) => {
                                    assert_eq!(
                                        content.restrictions.len(),
                                        1,
                                        "expected exactly one restriction"
                                    );
                                    let restriction = content.restrictions.first().unwrap();

                                    assert_eq!(
                                        (
                                            restriction.base.prefix.as_deref(),
                                            restriction.base.name.as_str()
                                        ),
                                        (Some("SOAP-ENC"), "Array"),
                                        "can only work with array restrictions for now"
                                    );
                                    assert_eq!(
                                        restriction.body.len(),
                                        1,
                                        "expected exactly one attribute"
                                    );

                                    let attributes = restriction
                                        .body
                                        .iter()
                                        .find_map(|(k, v)| (k.name == "attribute").then(|| v))
                                        .unwrap();

                                    let r#ref = attributes
                                        .iter()
                                        .find_map(|(k, v)| (k == "ref").then(|| v))
                                        .unwrap();

                                    assert_eq!(
                                        r#ref, "SOAP-ENC:arrayType",
                                        "can only work with array restrictions for now"
                                    );

                                    format!(
                                        r##"
                                        input.{field}.0.iter()
                                            .map(|x| format!("<item>{{}}</item>", html_escape::encode_text(&x.to_string())))
                                            .collect::<String>()
                                        "##,
                                        field = quote_literals(&part.name),
                                    )
                                }
                                ComplexBody::All(_content) => {
                                    format!(
                                        r##"todo!(r#"Serialize complex type: {{}}"#, {:?})"##,
                                        format!("{:#?}", complex_type)
                                    )
                                }
                            }
                        } else {
                            format!("html_escape::encode_text(&input.{field}.to_string())", field = quote_literals(&part.name))
                        };

                        format!(
                            "{name} = {value}, ",
                            name = part.name,
                            value = value
                        )
                    })
                    .collect();

                let body: String = input_message
                    .parts
                    .iter()
                    .map(|part| format!("<{name}>{{{name}}}</{name}>", name = part.name))
                    .collect();

                let return_message = self
                    .wsdl
                    .messages
                    .iter()
                    .find(|message| message.name == output_type.name)
                    .unwrap();

                let fields: String = return_message
                    .parts
                    .iter()
                    .map(|part| {
                        format!(
                            "{name}: {{
                                let node = super::field(node, {name:?});
                                {parser}
                            }}, ",
                            name = part.name,
                            parser = self.build_parser(state, &part.r#type)
                        )
                    })
                    .collect();

                let body = format!(
                    r##"
let message = format!(r#"<ns1:{tag} {encoding_style}>{body}</ns1:{tag}>"#, {args});
let res = self.send_soap_message(&message).await?;


// let res = include_str!("/home/soruh/tmp/temp.xml");

// println!("{{}}", res);

let document = ::roxmltree::Document::parse(&res).unwrap();
let envelope = document.root().children().find(is_element_with_prefix("SOAP-ENV", "Envelope")).unwrap();
let body = envelope.children().find(is_element_with_prefix("SOAP-ENV", "Body")).unwrap();
let message = body.children().find(is_element_with_prefix("ns1", {message_name:?})).unwrap();

let node = message;

Ok({return_type} {{
    {fields}
}})
"##,
                    tag = operation.name,
                    encoding_style = encoding_style
                        .map(|encoding_style| format!(
                            "SOAP-ENV:encodingStyle={:?}",
                            encoding_style
                        ))
                        .unwrap_or_else(String::new),
                    body = body,
                    args = args,
                    message_name = operation.output.as_ref().unwrap().name,
                    return_type = return_type.to_camel(),
                    fields = fields,
                );

                method.line(body);
            }
        }

        services
    }
}

fn quote_literals(ident: &str) -> Cow<str> {
    const LITERALS: &[&str] = &["type"];

    if LITERALS.contains(&ident) {
        Cow::from(format!("r#{}", ident))
    } else {
        Cow::from(ident)
    }
}

#[test]
fn test_generator() {
    use std::io::Read;

    let mut input = String::new();
    std::fs::File::open("ilias.wsdl")
        .unwrap()
        .read_to_string(&mut input)
        .unwrap();

    let wsdl = WsdlFile::parse(&input);

    // println!("{:#?}", &wsdl);

    let generator = Generator::new(wsdl);
    let mut state = State::new();

    generator.generate(&mut state);
}
